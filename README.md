# Programming Comprehension #

This README would normally document whatever steps are necessary to get your application up and running.

### Data Collection ###

The source code is in folder "ActivityCollecter".

The execute file is "ActivityCollecterConsole.exe" which is in folder "ActivityCollecter/Debug".

It can collect your interactions with applications. 

You can config the wanted applications in txt file "config.txt". 

You just need to run it by click "ActivityCollecterConsole.exe" and it need to run continually in order to collect your interactions

### Data Analysis ###

The source code is written in python and you can find it in folder "analysis".

First, we use "pc_evalutation.py" to identify different activities and compute their time statistics. Then the statistics are written into a json file for each participant. 

Second, we use "json_parer.py" to get readable excel/csv files. 


### Collected Programming Comprehension Data ###

Due to the infomration policy of our partern company, the raw collected data is not uploaded into this repository.



### Contact Information ###

If you have any questions, please contact us.

email: lingfengbao@zju.edu.cn